Sobre o teste.

As tecnologias utilizadas para o desenvolvimento do teste foram, Java 8, JHipster, Spring Boot e banco Postgres.


-Para iniciar deve ser instalado o servidor Node.js.

-Na interface, a opção API mostra todos os endpoints da aplicação usando o Swagger que é uma API RESTful de documentação interativa, com os endpoints listados e prontos para testar e usar em outros dispositivos.

-Por default do framework temos todos os testes de performance, como pode ser observado no pacote test, é testado os cruds, tempos de execução.

-No menu Métricas mostra como está sua máquina virtual Java (JVM). Isso é muito útil para quando para produção.

Devido ao experimento, não foi usado o spring boot cru, foi usada uma implementação do JHipster. Este foi um experimento de sucesso.

Para versiona, documenta, testa foi utilizado o swagger.

Sobre as regras:

A validação de um voto por associado foi feita via banco, criada uma UniqueConstraint na entity para restringir. Esta na modelagem. Esta solução foi desenvolvida devido ao tempo.

Ao abrir sessão de votação é definida hora inicio e hora fim, e no momento da votação um serviço, não rest, é consultado para verifica da data.

A pauta tem uma sessão de votação, e a sessão tem a votação em si. 


